using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Items/Create Generic Item")]
public class ItemDefinition : ScriptableObject
{
    public new string name;
    public int maxStackSize;
    public float weight;

    public GameObject worldObject;
    public GameObject worldVisuals;

    public GameObject UIObject;
    public Sprite UIIcon;

    public enum Usability
    {
        None = 0, //0000
        Equippable = 1, //0001
        Consumable = 2, //0010
    }
    public Usability usablity = Usability.None;

    public Inventory.Slot.SlotType equipSlot = Inventory.Slot.SlotType.None;

    public enum ItemType
    {
        Junk = 0, //0000
        Weapon = 1, //0001
        Wearable = 2, //0010
        Consumable = 3, //0011
        Supply = 4, //0100

    }
    public ItemType type = ItemType.Junk;

    [System.Serializable]
    public class StatToModify
    {
        public StatsSystem.Stat.StatType stat;
        public float modifyValue;
    }
    public List<StatToModify> statsToModify = new List<StatToModify>();

    public bool IsType(ItemType itemType)
    {
        return (int)(type & itemType) != 0;
    }

}
