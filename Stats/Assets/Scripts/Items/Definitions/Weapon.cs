using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Items/Weapon/Create Generic Weapon")]
public class Weapon : ItemDefinition
{
    public int damage;
    public float attackSpeed;
    public bool twoHanded;

    public enum WeaponType
    {
        Melee = 1, //0001
        Ranged = 2, //0010

    }
    public WeaponType weaponType;

    public enum DamageType
    {
        Slashing = 1, //0001
        Bludgeoning = 2, //0010
        Piercing = 3, //0011

    }
    public DamageType damageType;

    private void Awake()
    {
        type = ItemType.Weapon;
        usablity = Usability.Equippable;
    }
}
