using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Armor", menuName = "Items/Armor/Create Generic Armor")]
public class Armor : ItemDefinition
{
    private void Awake()
    {
        type = ItemType.Wearable;
        usablity = Usability.Equippable;
    }
}
