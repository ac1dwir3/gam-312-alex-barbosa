using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InventoryUI : MonoBehaviour
{
    public Inventory playerInventory;
    [SerializeField] RectTransform contentWindow;
    [SerializeField] TextMeshProUGUI weightText; 

    List<UIItem> inventoryUIContents = new List<UIItem>();

    public List<UISlot> equipmentUIPositions = new List<UISlot>();

    private void Awake()
    {
        playerInventory.AddItemPickupCallback(PickedUpItem);
        playerInventory.AddItemDropCallback(DroppedItem);
        playerInventory.AddItemEquipCallback(EquippedItem);
        playerInventory.AddItemUnequipCallback(UnequippedItem);
        playerInventory.gameObject.GetComponent<StatsSystem>().AddStatChangedCallback(UpdateWeightText);
    }

    // Start is called before the first frame update
    void Start()
    {
        weightText.text = "Carry Weight: " + playerInventory.gameObject.GetComponent<StatsSystem>().stats[StatsSystem.Stat.StatType.CarryWeight].currentValue + "/" + playerInventory.gameObject.GetComponent<StatsSystem>().stats[StatsSystem.Stat.StatType.CarryWeight].maxValue;
    }

    // Update is called once per frame
    void Update()
    {

    }

    //Called when an item is added to the player's inventory
    public void PickedUpItem(Item item)
    {
        GameObject inventoryItem = Instantiate(item.definition.UIObject, contentWindow);

        inventoryItem.GetComponent<UIItem>().SetUIItemVisuals(item);

        inventoryUIContents.Add(inventoryItem.GetComponent<UIItem>());
    }
    //Called when an item is removed from the player's inventory
    public void DroppedItem(Item item)
    {
        foreach (UIItem _item in inventoryUIContents)
        {
            if (_item.itemData.definition == item.definition)
            {
                Destroy(_item.gameObject);
                inventoryUIContents.Remove(_item);
                break;
            }
        }
    }
    //Called when an item is equipped in the player's inventory
    public void EquippedItem(Item item)
    {
        foreach (UISlot slot in equipmentUIPositions)
        {
            if (item.definition.equipSlot == slot.slotType)
            {
                foreach (UIItem _item in inventoryUIContents)
                {
                    if (_item.itemData.definition == item.definition)
                    {
                        _item.transform.SetParent(slot.transform);
                        _item.GetComponent<RectTransform>().localPosition = Vector3.zero;
                        _item.GetComponent<RectTransform>().sizeDelta = new Vector2(slot.GetComponent<RectTransform>().rect.width, slot.GetComponent<RectTransform>().rect.height);
                        _item.GetComponent<DraggableUI>().enabled = false;
                        _item.GetComponent<UIItem>().SetUIEquipmentVisuals(item);
                    }
                }
            }
        }
    }
    //Called when an item is unequipped in the player's inventory
    public void UnequippedItem(Item item)
    {
        foreach (UISlot slot in equipmentUIPositions)
        {
            if (slot.slotType == item.definition.equipSlot)
            {
                slot.GetComponentInChildren<UIItem>().GetComponent<DraggableUI>().enabled = true;
                slot.GetComponentInChildren<UIItem>().SetUIItemVisuals(item);
                slot.GetComponentInChildren<UIItem>().transform.SetParent(contentWindow);
            }
        }
    }

    public void UpdateWeightText(StatsSystem.Stat stat)
    {
        if(stat.type == StatsSystem.Stat.StatType.CarryWeight)
        {
            weightText.text = "Carry Weight: " + stat.currentValue + "/" + stat.maxValue;
        }
    }
}
