using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenSlideDoor : MonoBehaviour
{
    BoxCollider doorCollider;

    List<GameObject> objectsInTriggerArea = new List<GameObject>();

    private void Start()
    {
        doorCollider = GetComponent<BoxCollider>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (!objectsInTriggerArea.Contains(other.GetComponentInParent<PlayerController>().gameObject))
                objectsInTriggerArea.Add(other.GetComponentInParent<PlayerController>().gameObject);
        }

        PlayerController playersController = other.GetComponentInParent<PlayerController>();

        if (playersController == null)
            return;

        if (playersController.isSliding)
        {
            doorCollider.enabled = false;
            doorCollider.GetComponent<MeshRenderer>().enabled = false;
            StartCoroutine(CloseDoor());
        }
    }

    public void OnTriggerStay(Collider other)
    {
        Rigidbody playersRb = null;

        if (other.CompareTag("Player"))
            playersRb = other.GetComponentInParent<Rigidbody>();

        if (playersRb == null)
            return;
        if (playersRb.gameObject.GetComponent<PlayerController>().slideTimeCounter <= 0)
            if (Mathf.Abs(playersRb.velocity.x) <= (playersRb.gameObject.GetComponent<StatsSystem>().GetCurrentValue(StatsSystem.Stat.StatType.MovementSpeed) * playersRb.gameObject.GetComponent<StatsSystem>().GetCurrentValue(StatsSystem.Stat.StatType.CrouchSpeedMultiplier) * 10))
            {
                doorCollider.enabled = true;
                doorCollider.GetComponent<MeshRenderer>().enabled = true;
            }
    }
    IEnumerator CloseDoor()
    {
        yield return new WaitForSeconds(1);
        doorCollider.enabled = true;
        doorCollider.GetComponent<MeshRenderer>().enabled = true;
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (objectsInTriggerArea.Contains(other.GetComponentInParent<PlayerController>().gameObject))
                objectsInTriggerArea.Remove(other.GetComponentInParent<PlayerController>().gameObject);
        }

        if (objectsInTriggerArea.Count <= 0)
        {
            doorCollider.enabled = true;
            doorCollider.GetComponent<MeshRenderer>().enabled = true;
        }
    }
}
