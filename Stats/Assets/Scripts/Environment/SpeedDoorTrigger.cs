using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedDoorTrigger : MonoBehaviour
{
    OpenSpeedDoor parent;

    // Start is called before the first frame update
    void Start()
    {
        parent = GetComponentInParent<OpenSpeedDoor>();
    }

    private void OnTriggerEnter(Collider other)
    {
        parent.OnTriggerEnter(other);
    }

    private void OnTriggerStay(Collider other)
    {
        parent.OnTriggerStay(other);
    }

    private void OnTriggerExit(Collider other)
    {
        parent.OnTriggerExit(other);
    }
}
