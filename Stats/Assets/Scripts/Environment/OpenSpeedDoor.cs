using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenSpeedDoor : MonoBehaviour
{
    BoxCollider doorCollider;
    List<GameObject> objectsInTriggerArea = new List<GameObject>();

    [ReadOnly, SerializeField] float minSpeed;
    [SerializeField] bool yVelocityInstead = false;

    private void Start()
    {
        doorCollider = GetComponent<BoxCollider>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (!objectsInTriggerArea.Contains(other.GetComponentInParent<PlayerController>().gameObject))
                objectsInTriggerArea.Add(other.GetComponentInParent<PlayerController>().gameObject);
        }

        minSpeed = GetComponent<StatsSystem>().GetMinValue(StatsSystem.Stat.StatType.MovementSpeed);
    }

    public void OnTriggerStay(Collider other)
    {
        Rigidbody otherRb = other.GetComponentInParent<Rigidbody>();

        if (otherRb == null)
            return;
        if (yVelocityInstead)
        {
            if (Mathf.Abs(otherRb.velocity.y) >= minSpeed)
            {
                doorCollider.enabled = false;
                doorCollider.GetComponent<MeshRenderer>().enabled = false;
            }
        }
        else
        {
            if (Mathf.Abs(otherRb.velocity.x) >= minSpeed)
            {
                doorCollider.enabled = false;
                doorCollider.GetComponent<MeshRenderer>().enabled = false;
            }
        }

    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (objectsInTriggerArea.Contains(other.GetComponentInParent<PlayerController>().gameObject))
                objectsInTriggerArea.Remove(other.GetComponentInParent<PlayerController>().gameObject);
        }

        if (objectsInTriggerArea.Count <= 0)
        {
            doorCollider.enabled = true;
            doorCollider.GetComponent<MeshRenderer>().enabled = true;
        }
    }
}
