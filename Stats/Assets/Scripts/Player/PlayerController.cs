using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(StatsSystem))]
public class PlayerController : MonoBehaviour
{
    #region Variables
    public Rigidbody rb;

    #region Movement Variables
    [Header("Movement")]
    [HideInInspector] public RaycastHit slopeHit;
    [HideInInspector] public float movementMultiplier = 10;
    [HideInInspector] public bool isSprinting = false;

    [Header("Crouching")]
    [HideInInspector] public bool isCrouching = false;
    bool canStand = true;
    [SerializeField] Transform headCheck;
    [SerializeField] float headCheckDistance = 1f;

    [Header("Jumping ")]
    [SerializeField] float airDrag = 2;
    [SerializeField] [Range(0.01f, 1)] float airMovementMultiplier = 0.3f;

    [Header("Sliding")]
    [SerializeField] [Tooltip("Velocity in the x direction must be greater than this threshold to slide (account for Movement Speed)")] float slideSpeedMin = 12;
    [SerializeField] [Tooltip("How long (in seconds) you ignore player input for while sliding")] float slideTime = 0.5f;
    [HideInInspector] public float slideTimeCounter;
    [HideInInspector] public bool isSliding = false;
    #endregion

    #region Gravity/Falling Variables
    [Header("Grounding/Falling")]
    [SerializeField] Transform groundCheck;
    [SerializeField] float groundDistance = 0.1f;
    public LayerMask groundLayer = default;
    [HideInInspector] public bool isGrounded;
    #endregion

    #region Visual Variables
    [Header("Visuals")]
    public GameObject standingModel;
    public GameObject crouchModel;
    #endregion

    public StatsSystem myStats;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        myStats.ResetAll();
    }

    // Update is called once per frame
    void Update()
    {
        ControlDrag();
    }

    private void FixedUpdate()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, -groundDistance, groundLayer);
        if (!isGrounded && rb.useGravity)
        {
            rb.AddForce(Physics.gravity * myStats.GetCurrentValue(StatsSystem.Stat.StatType.GravityMultiplier), ForceMode.Acceleration);
            Debug.DrawRay(transform.position, rb.velocity.normalized, Color.green);
        }

        canStand = !Physics.CheckCapsule(headCheck.position, new Vector3(headCheck.position.x, headCheck.position.y + headCheckDistance, headCheck.position.z), .45f, groundLayer);
    }

    //private void OnDrawGizmos()
    //{
    //    if (canStand)
    //        Gizmos.color = Color.green;
    //    else
    //        Gizmos.color = Color.red;

    //    Gizmos.DrawWireCube(new Vector3(headCheck.position.x, headCheck.position.y + headCheckDistance/2, headCheck.position.z), new Vector3(.95f, headCheckDistance, 0));
    //}

    #region Methods
    /// <summary>
    /// Changes rigidbody's drag based on whether the player is in the air or on the ground
    /// </summary>
    void ControlDrag()
    {
        if (isGrounded) //If on the ground
            rb.drag = myStats.GetCurrentValue(StatsSystem.Stat.StatType.GroundDrag); //Apply ground drag
        else //If not on the ground (in the air)
            rb.drag = airDrag; //Apply air drag
    }
    /// <summary>
    /// Detects checks the angle of the surface the player is standing on
    /// </summary>
    /// <returns>Whether or not the player is standing on a sloped surface</returns>
    public bool OnSlope()
    {
        if(Physics.Raycast(groundCheck.position, Vector3.down, out slopeHit, groundDistance, groundLayer)) //If there is a surface below the player
        {
            if (slopeHit.normal != Vector3.up) //If the surface is not perfectly flat
                return true; //Player is standing on a slope
            else //If the surface is perfectly flat
                return false; //Player is standing on a flat surface (not on a slope)
        }

        return false; //If there is no surface below the player then the player is not on a slope
    }
    public void Move(Vector3 direction)
    {
        if (myStats.GetCurrentValue(StatsSystem.Stat.StatType.CarryWeight) >= myStats.GetMaxValue(StatsSystem.Stat.StatType.CarryWeight)) //If over-encumbered
            movementMultiplier = 5; //movementSpeed is halved
        else
            movementMultiplier = 10;

        if(isGrounded && !isSprinting && !isCrouching) //If grounded, NOT sprinting, and NOT crouching
            rb.AddForce(direction * (myStats.GetCurrentValue(StatsSystem.Stat.StatType.MovementSpeed) * movementMultiplier), ForceMode.Acceleration); //Move with normal speed
        else if(isGrounded && isSprinting && !isCrouching) //If grounded, sprinting, but not crouching
            rb.AddForce(direction * ((myStats.GetCurrentValue(StatsSystem.Stat.StatType.MovementSpeed) * movementMultiplier) * myStats.GetCurrentValue(StatsSystem.Stat.StatType.SprintSpeedMultiplier)), ForceMode.Acceleration); //Move with sprint speed modifier
        else if (isGrounded && isCrouching) //If grounded and crouching (regardless if sprinting)
            rb.AddForce(direction * ((myStats.GetCurrentValue(StatsSystem.Stat.StatType.MovementSpeed) * movementMultiplier) * myStats.GetCurrentValue(StatsSystem.Stat.StatType.CrouchSpeedMultiplier)), ForceMode.Acceleration); //Move with crouch speed modifier
        else if(!isGrounded) //If in the air
            rb.AddForce(direction * ((myStats.GetCurrentValue(StatsSystem.Stat.StatType.MovementSpeed) * movementMultiplier) * airMovementMultiplier), ForceMode.Acceleration); //Move with air speed ("resistance") modifier
    }
    public void Jump()
    {
        if(isGrounded) //No jumping while in mid-air
        {
            rb.velocity = new Vector3(rb.velocity.x, 0, 0);

            if (!isCrouching) //If not crouching
                rb.AddForce(transform.up * myStats.GetCurrentValue(StatsSystem.Stat.StatType.JumpForce), ForceMode.Impulse); //Jump normally
            else //If crouching
                rb.AddForce(transform.up * (myStats.GetCurrentValue(StatsSystem.Stat.StatType.JumpForce) * myStats.GetCurrentValue(StatsSystem.Stat.StatType.CrouchJumpMultiplier)), ForceMode.Impulse); //Crouch hop
        }
    }
    public void Crouch()
    {
        isCrouching = true;
        isSliding = false;
        //Change models
        standingModel.SetActive(false);
        crouchModel.SetActive(true);

        if (isGrounded && Mathf.Abs(rb.velocity.x) > slideSpeedMin) //If grounded and moving fast enough
            Slide();

    }
    public void Slide()
    {
        isSliding = true;
        slideTimeCounter = slideTime; //Starts countdown that ignores player input

        rb.velocity = new Vector3(rb.velocity.normalized.x * slideSpeedMin, rb.velocity.y);

        Vector3 slideDirection = rb.velocity.normalized; //Records current movement direction for slide direction
        slideDirection.y = 0; //Don't slide upward

        rb.AddForce(slideDirection * myStats.GetCurrentValue(StatsSystem.Stat.StatType.SlideForce), ForceMode.Impulse); //Apply Slide/Dash/Dive Force
    }
    public void Stand()
    {
        if(canStand) //If player has enough room above them to stand
        {
            isCrouching = false;
            isSliding = false;
            //Change models
            crouchModel.SetActive(false);
            standingModel.SetActive(true);
        }
    }
    #endregion
}
