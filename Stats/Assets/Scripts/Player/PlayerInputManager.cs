using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerController))]
[RequireComponent(typeof(WallJump))]
public class PlayerInputManager : MonoBehaviour
{
    #region Variables
    PlayerController controller;

    #region Input Variables
    [HideInInspector] public Vector2 movementInput = Vector2.zero;
    Vector3 movementDirection;
    Vector3 slopeMovementDirection;

    [HideInInspector] public bool jumped = false;
    bool sprinting = false;
    bool crouching = false;

    bool pausedPressed = false;
    bool inventoryPressed = false;
    #endregion

    WallJump wallJump;

    #endregion
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<PlayerController>();
        wallJump = GetComponent<WallJump>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.gameState == GameManager.GameState.Gameplay) //While in Gameplay
        {
            //Flips player based on movement direction
            if (movementInput.x > 0)
                transform.localScale = Vector3.one; //Faces right
            if (movementInput.x < 0)
                transform.localScale = new Vector3(-1, 1, 1); //Faces left

            if (wallJump.wallJumpCounter <= 0 && controller.slideTimeCounter <= 0) //Ignores input while wall jumping and sliding
            {
                //Calculates movement direction
                movementDirection = transform.right * movementInput.x;
                Debug.DrawRay(transform.position, movementDirection, Color.red); //Visualizes movement direction
                slopeMovementDirection = Vector3.ProjectOnPlane(movementDirection, controller.slopeHit.normal); //Adjusts movement direction when on a slope to be parallel to the slope
                Debug.DrawRay(transform.position, slopeMovementDirection, Color.blue); //Visualizes sloped movement direction

                if (crouching) //if crouch button is being pressed
                {
                    if (!controller.isCrouching) //If player not already crouching
                        controller.Crouch();
                }
                else //If crouch button not being pressed
                    controller.Stand();

                if (sprinting) //If sprint button is being pressed
                    controller.isSprinting = true;
                else //If sprint button is not being pressed
                    controller.isSprinting = false;

            }
            else
            {
                wallJump.wallJumpCounter -= Time.deltaTime; //Counts down wall jump timer until we can take input again
                controller.slideTimeCounter -= Time.deltaTime; //Counts down slide timer until we can take input again
            }

            if (inventoryPressed) //If inventory button was pressed while in Gameplay
            {
                for(int i = 0; i < GameManager.instance.inventoryUI.transform.childCount; i++)
                {
                    GameManager.instance.inventoryUI.transform.GetChild(i).gameObject.SetActive(true); //Show the UI
                }
                GameManager.instance.gameState = GameManager.GameState.InInventory; //Set game's state to In Inventory
                inventoryPressed = false;

                
            }
            if (pausedPressed) //If pause button was pressed while in Gameplay
            {
                GameManager.instance.gameState = GameManager.GameState.Paused; //Set game's state to Paused
                pausedPressed = false;
            }


        }
        else if (GameManager.instance.gameState == GameManager.GameState.InInventory) //While in Inventory
        {
            if (inventoryPressed) //If inventory button was pressed while already in the inventory
            {
                for (int i = 0; i < GameManager.instance.inventoryUI.transform.childCount; i++)
                {
                    GameManager.instance.inventoryUI.transform.GetChild(i).gameObject.SetActive(false); //Hide the UI
                }
                GameManager.instance.gameState = GameManager.GameState.Gameplay; //Set game's state to Gameplay
                inventoryPressed = false;
            }
            if (pausedPressed) //If pause button was pressed while in the inventory
            {
                GameManager.instance.gameState = GameManager.GameState.Paused; //Set game's state to Paused
                pausedPressed = false;
            }

        }
        else if (GameManager.instance.gameState == GameManager.GameState.Paused) //While Paused
        {
            if (pausedPressed) //If pause button was pressed while already paused
            {
                GameManager.instance.gameState = GameManager.GameState.Gameplay;
                pausedPressed = false;
            }
        }
    }

    private void FixedUpdate()
    {
        if (GameManager.instance.gameState == GameManager.GameState.Gameplay) //While in Gameplay
        {
            if (wallJump.wallJumpCounter <= 0 && controller.slideTimeCounter <= 0) //Ignores input while wall jumping and sliding
            {
                if (jumped) //If jump button is being pressed pressed
                    controller.Jump();

                if (movementDirection.magnitude >= 0.1f) //If movement input threshold past
                {
                    if (!controller.OnSlope()) //If on a flat surface (not sloped)
                    {
                        //Allows for variable movement speed
                        if (movementDirection.magnitude >= 1)
                            controller.Move(movementDirection.normalized);
                        else
                            controller.Move(movementDirection);
                    }
                    else //If on a sloped surface
                    {
                        //Allows for variable movement speed
                        if (movementDirection.magnitude >= 1)
                            controller.Move(slopeMovementDirection.normalized);
                        else
                            controller.Move(slopeMovementDirection);
                    }
                }
            }
        }
    }

    #region Input Reader Methods
    public void OnMove(InputAction.CallbackContext context)
    {
        movementInput = context.ReadValue<Vector2>();
    }
    public void OnCrouch(InputAction.CallbackContext context)
    {
        crouching = context.action.triggered;
    }
    public void OnJump(InputAction.CallbackContext context)
    {
        jumped = context.action.triggered;
    }
    public void OnSprint(InputAction.CallbackContext context)
    {
        sprinting = context.action.triggered;
    }
    public void OnPause(InputAction.CallbackContext context)
    {
        pausedPressed = context.action.triggered;
    }
    public void OnInventory(InputAction.CallbackContext context)
    {
        inventoryPressed = context.action.triggered;
    }
    #endregion
}
