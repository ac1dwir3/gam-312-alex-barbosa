using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallJump : MonoBehaviour
{
    #region Variables
    [SerializeField] PlayerController controller;
    [SerializeField] PlayerInputManager input;

    [Header("Wall Sticking")]
    [SerializeField] Transform wallGrabPoint;
    bool canGrab;
    bool isGrabbing;

    [Header("Wall Jumping")]
    [SerializeField] [Min(10f)] Vector2 wallJumpForce;
    [Space]
    [SerializeField] [Range(0.05f, 0.3f)] [Tooltip("How long (in seconds) player input is ignored for after a wall jump")] float wallJumpTime = 0.1f;
    [HideInInspector] public float wallJumpCounter;
    bool wallJumpPreped = false;

    float storedGravity;

    [SerializeField] GameObject wallSlideParticles;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<PlayerController>();
        storedGravity = controller.myStats.GetCurrentValue(StatsSystem.Stat.StatType.GravityMultiplier);

        controller.myStats.AddStatChangedCallback(UpdateGravity);
    }

    // Update is called once per frame
    void Update()
    {
        isGrabbing = false;
        if(canGrab && !controller.isGrounded) //If there is a wall in front of player and player is not on the ground
        {
            if((input.movementInput.x > 0 && transform.localScale.x == 1) || (input.movementInput.x < 0 && transform.localScale.x == -1)) //If facing the wall and moving towards it
                isGrabbing = true;
        }

    }

    private void FixedUpdate()
    {
        canGrab = Physics.CheckSphere(wallGrabPoint.position, 0.3f, controller.groundLayer) && !controller.isCrouching; //Can't grab the wall if the player is not facing the wall or if they're crouching

        if (isGrabbing) //If player is grabbing a wall
        {
            controller.rb.useGravity = false; //Deactivates Gravity
            controller.myStats.stats[StatsSystem.Stat.StatType.GravityMultiplier].currentValue = 0; //Deactivates Artificial Gravity

            controller.rb.AddForce(Vector3.down * controller.myStats.GetCurrentValue(StatsSystem.Stat.StatType.WallSlideForce), ForceMode.Force); //Slide player down the wall
            wallSlideParticles.SetActive(true); //Activates particles when sliding

            if (!input.jumped) //If the player let go of the jump button (prevents player from holding jump to walljump)
                wallJumpPreped = true;

            if (input.jumped && wallJumpPreped) //If the player can walljump and they pressed the jump button
            {
                wallJumpCounter = wallJumpTime; //Starts countdown that ignores player input
                controller.rb.velocity = Vector3.zero;
                controller.rb.AddForce(new Vector2(-input.movementInput.x * wallJumpForce.x, wallJumpForce.y), ForceMode.Impulse); //Applys walljump force
            }
        }
        else //If the player is not grabbing a wall
        {
            controller.rb.useGravity = true; //Activates Gravity
            controller.myStats.stats[StatsSystem.Stat.StatType.GravityMultiplier].currentValue = storedGravity; //Activates Artificial Gravity
            wallJumpPreped = false; //Unpreps the next walljump
            wallSlideParticles.SetActive(false); //Deactivates particles
        }

    }

    public void UpdateGravity(StatsSystem.Stat stat)
    {
        if(stat.type == StatsSystem.Stat.StatType.GravityMultiplier)
        {
            storedGravity = controller.myStats.GetCurrentValue(StatsSystem.Stat.StatType.GravityMultiplier);
        }
    }
}
