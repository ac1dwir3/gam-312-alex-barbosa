using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public enum GameState
    {
        Gameplay = 0, //0000
        InInventory = 1, //0001
        Paused = 2, //0010
    }
    public GameState gameState = GameState.Gameplay;

    public InventoryUI inventoryUI;

    public Canvas playerUICanvas;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
