using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsSystem : MonoBehaviour
{
    [System.Serializable]
    public class Stat
    {
        public float initialValue;
        [ReadOnly] public float currentValue;
        public float minValue;
        public float maxValue;
        public bool greaterThanMaxAllowed;
        public bool lessThanMinAllowed;
        public enum StatType
        {
            JumpForce = 1,
            MovementSpeed = 2,
            CarryWeight = 3,
            SlideForce = 4,
            SprintSpeedMultiplier = 5,
            CrouchSpeedMultiplier = 6,
            CrouchJumpMultiplier = 7,
            GroundDrag = 8, //Can affect slipperiness of stat bearer
            GravityMultiplier = 9, //Can make stat bearer fall faster/slower
            WallSlideForce = 10, //Can make stat bearer slide down walls faster, climb up walls, or stick in place
        }
        public StatType type;
    }
    public Dictionary<Stat.StatType, Stat> stats = new Dictionary<Stat.StatType, Stat>();

    [SerializeField] List<Stat> initialStatValues = new List<Stat>();

    #region Observer Pattern / Delegates
    public delegate void dStatChanged(Stat stat);
    dStatChanged statChangedCallback;

    public void AddStatChangedCallback(dStatChanged someFunc)
    {
        statChangedCallback += someFunc;
    }
    #endregion

    private void Start()
    {
        for (int i = 0; i < initialStatValues.Count; i++)
        {
            initialStatValues[i].currentValue = initialStatValues[i].initialValue;
            stats.Add(initialStatValues[i].type, initialStatValues[i]);
        }
    }

    /// <param name="statToGet">The desired stat</param>
    /// <returns>The current value of the stat</returns>
    public float GetCurrentValue(Stat.StatType statToGet)
    {
        if (stats.ContainsKey(statToGet))
            return stats[statToGet].currentValue;
        else
            Debug.LogWarning(this.name + " does not have that stat!");

        return 0;
    }
    /// <param name="statToGet">The desired stat</param>
    /// <returns>The maximum value the stat can be</returns>
    public float GetMaxValue(Stat.StatType statToGet)
    {
        if (stats.ContainsKey(statToGet))
            return stats[statToGet].maxValue;
        else
            Debug.LogWarning(this.name + " does not have that stat!");

        return 0;
    }
    /// <param name="statToGet">The desired stat</param>
    /// <returns>The minimum value the stat can be</returns>
    public float GetMinValue(Stat.StatType statToGet)
    {
        if (stats.ContainsKey(statToGet))
            return stats[statToGet].minValue;
        else
            Debug.LogWarning(this.name + " does not have that stat!");

        return 0;
    }

    /// <summary>
    /// Changes the current value of a stat
    /// </summary>
    /// <param name="statToChange">The desired stat you want to change</param>
    /// <param name="valueOfChange">The amount of change you want to make to the stat's current value (negative numbers for subtraction/positive numbers for addition)</param>
    public void ChangeValue(Stat.StatType statToChange, float valueOfChange)
    {
        bool statExists = false;
        if (stats.ContainsKey(statToChange))
            statExists = true;

        if (statExists)
        {
            if(!stats[statToChange].greaterThanMaxAllowed && !stats[statToChange].lessThanMinAllowed)
            {
                if ((stats[statToChange].currentValue + valueOfChange) >= stats[statToChange].maxValue) //If the stat change is greater than the stat's max value
                    stats[statToChange].currentValue = stats[statToChange].maxValue; //Set the stat's current value to it's max value
                else if ((stats[statToChange].currentValue + valueOfChange) <= stats[statToChange].minValue) //If the stat change is less than the stat's min value
                    stats[statToChange].currentValue = stats[statToChange].minValue; //Set the stat's current value to it's min value
                else //If the stat change is NOT greater than it's max value AND is NOT less than it's min value
                    stats[statToChange].currentValue += valueOfChange; //Set current value of the stat to the new value
            }
            else if (!stats[statToChange].greaterThanMaxAllowed) //If the stat is NOT allowed to surpass it's max and min values
            {
                if ((stats[statToChange].currentValue + valueOfChange) >= stats[statToChange].maxValue) //If the stat change is greater than the stat's max value
                    stats[statToChange].currentValue = stats[statToChange].maxValue; //Set the stat's current value to it's max value
                else //If the stat change is NOT greater than it's max value
                    stats[statToChange].currentValue += valueOfChange; //Set current value of the stat to the new value
            }
            else if (!stats[statToChange].lessThanMinAllowed)
            {
                if ((stats[statToChange].currentValue + valueOfChange) <= stats[statToChange].minValue) //If the stat change is less than the stat's min value
                    stats[statToChange].currentValue = stats[statToChange].minValue; //Set the stat's current value to it's min value
                else //If the stat change is NOT less than it's min value
                    stats[statToChange].currentValue += valueOfChange; //Set current value of the stat to the new value
            }
            else //If the stat IS allowed to surpass it's max AND min values
            {
                stats[statToChange].currentValue += valueOfChange; //Set current value of the stat to the new value
            }

            statChangedCallback(stats[statToChange]);
        }
        else
            Debug.LogWarning(this.name + " does not have that stat!");
    }
    /// <summary>
    /// Resets a stat's current value to it's initial value
    /// </summary>
    /// <param name="statToReset">The desired stat you want reset</param>
    public void ResetStat(Stat.StatType statToReset)
    {
        bool statExists = false;
        if (stats.ContainsKey(statToReset))
            statExists = true;

        if (statExists)
        {
            for (int i = 0; i < initialStatValues.Count; i++)
            {
                if (initialStatValues[i].type == statToReset)
                {
                    stats[statToReset].currentValue = initialStatValues[i].initialValue;
                    statChangedCallback(stats[statToReset]);
                    break;
                }
            }
        }
        else
            Debug.LogWarning(this.name + " does not have that stat!");
    }
    /// <summary>
    /// Resets all stats to their initial values
    /// </summary>
    public void ResetAll()
    {
        for (int i = 0; i < stats.Count; i++)
        {
            stats[initialStatValues[i].type].currentValue = initialStatValues[i].initialValue;
            statChangedCallback(stats[initialStatValues[i].type]);
        }
    }
}
