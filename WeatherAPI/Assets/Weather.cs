using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using SimpleJSON;
using TMPro;

public class Weather : MonoBehaviour
{
    [Header("Location Data")]
    [SerializeField] string city = "Hooksett";
    [SerializeField] string stateInitials = "NH";
    [SerializeField] string country = "USA";

    [Header("Environment Changes")]
    [SerializeField] Light directionalLight;
    [SerializeField] List<ParticleSystem> weatherParticleSystems = new List<ParticleSystem>();
    [SerializeField] List<Material> skyboxes = new List<Material>();

    [Header("UI Changes")]
    [SerializeField] TextMeshProUGUI weatherTypeText;
    [SerializeField] TextMeshProUGUI lightColorText;
    [SerializeField] TextMeshProUGUI lightIntensityText;
    [SerializeField] Image weatherIconImage;
    [SerializeField] List<Sprite> weatherIcons = new List<Sprite>();


    // Start is called before the first frame update
    void Start()
    {
        if (directionalLight == null)
            FindObjectOfType(typeof(Light));

        StartCoroutine(GetWeather());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            ChangeWeather("Clear");
        if (Input.GetKeyDown(KeyCode.Alpha2))
            ChangeWeather("Clouds");
        if (Input.GetKeyDown(KeyCode.Alpha3))
            ChangeWeather("Drizzle");
        if (Input.GetKeyDown(KeyCode.Alpha4))
            ChangeWeather("Rain");
    }

    IEnumerator GetWeather()
    {
        string url = "http://api.openweathermap.org/data/2.5/weather?q=" + city.ToLower() + "," + stateInitials.ToLower() + "," + country.ToLower() + "&appid=6b7e51c8b1f9fc48952d7f149fedcc85";

        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            yield return webRequest.SendWebRequest();
            string websiteText = webRequest.downloadHandler.text;
            SimpleJSON.JSONNode data = SimpleJSON.JSON.Parse(websiteText);
            ChangeWeather(data["weather"][0]["main"]);

            yield return new WaitForSecondsRealtime(600f); //Waits 10 minutes
            StartCoroutine(GetWeather()); //Restarts itself (Updating the game's weather every 10 minutes to reflect the current weather)
        }
    }

    void ChangeWeather(string weatherType)
    {
        switch (weatherType)
        {
            case "Clear":
                directionalLight.intensity = .5f;
                Color sunColor = new Vector4(.86f, .82f, .43f, 1);
                directionalLight.color = sunColor;
                weatherTypeText.text = "Weather Type: Clear";
                weatherIconImage.sprite = weatherIcons[0];
                foreach (ParticleSystem ps in weatherParticleSystems)
                {
                    ps.Stop();
                }
                RenderSettings.skybox = skyboxes[0];
                break;

            case "Clouds":
                directionalLight.intensity = 0.4f;
                directionalLight.color = Color.gray;
                weatherTypeText.text = "Weather Type: Clouds";
                weatherIconImage.sprite = weatherIcons[1];
                foreach(ParticleSystem ps in weatherParticleSystems)
                {
                    ps.Stop();
                }
                RenderSettings.skybox = skyboxes[1];
                break;

            case "Drizzle":
                directionalLight.intensity = 0.4f;
                directionalLight.color = Color.grey;
                weatherTypeText.text = "Weather Type: Drizzle";
                weatherIconImage.sprite = weatherIcons[2];
                foreach (ParticleSystem ps in weatherParticleSystems)
                {
                    ps.Stop();
                }
                weatherParticleSystems[0].Play();
                RenderSettings.skybox = skyboxes[1];
                break;

            case "Rain":
                directionalLight.intensity = 0.2f;
                directionalLight.color = Color.grey;
                weatherTypeText.text = "Weather Type: Rain";
                weatherIconImage.sprite = weatherIcons[3];
                foreach (ParticleSystem ps in weatherParticleSystems)
                {
                    ps.Stop();
                }
                weatherParticleSystems[1].Play();
                RenderSettings.skybox = skyboxes[2];
                break;
        }

        lightIntensityText.text = "Light Intensity: " + directionalLight.intensity;
        lightColorText.text = "Light Color: " + directionalLight.color;
    }
}
