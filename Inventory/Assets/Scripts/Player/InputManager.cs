using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    public delegate void InventoryPressed();
    public event InventoryPressed inventoryPressed;
    public delegate void AddItemPressed(ItemDefinition itemDefinition, int count);
    public event AddItemPressed addItemPressed;

    public void OnInventory(InputAction.CallbackContext context)
    {
        inventoryPressed();
    }

    public void OnAddItemToInventory(InputAction.CallbackContext context)
    {
        if (context.performed)
            addItemPressed(GameManager.instance.itemToAdd, 1);
    }
}
