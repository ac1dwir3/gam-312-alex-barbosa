using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public InputManager inputManager;

    public InventoryUI inventoryUI;
    public ItemDefinition itemToAdd;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }
    private void OnEnable()
    {
        inputManager.inventoryPressed += ToggleInventoryUI;
    }
    private void OnDisable()
    {
        inputManager.inventoryPressed -= ToggleInventoryUI;
    }

    public void ToggleInventoryUI()
    {
        for(int i = 0; i < inventoryUI.transform.childCount; i++)
        {
            inventoryUI.transform.GetChild(i).gameObject.SetActive(!inventoryUI.transform.GetChild(i).gameObject.activeSelf);
        }
    }
}
