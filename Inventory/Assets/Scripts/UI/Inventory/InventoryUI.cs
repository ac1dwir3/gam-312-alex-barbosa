using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    public Inventory playerInventory;
    [SerializeField] RectTransform contentWindow;

    List<UIItem> inventoryUIContents = new List<UIItem>();

    public List<UISlot> equipmentUIPositions = new List<UISlot>();

    private void OnEnable()
    {
        playerInventory.newItemPickupCallback += PickedUpNewItem;
        playerInventory.itemPickupCallback += PickedUpItem;
        playerInventory.itemDropCallback += DroppedItem;
        playerInventory.itemEquipCallback += EquippedItem;
        playerInventory.itemUnequipCallback += UnequippedItem;
    }
    private void OnDisable()
    {
        playerInventory.newItemPickupCallback -= PickedUpNewItem;
        playerInventory.itemPickupCallback -= PickedUpItem;
        playerInventory.itemDropCallback -= DroppedItem;
        playerInventory.itemEquipCallback -= EquippedItem;
        playerInventory.itemUnequipCallback -= UnequippedItem;
    }

    //Called when a new item is added to the player's inventory
    public void PickedUpNewItem(Item item)
    {
        GameObject inventoryItem = Instantiate(item.definition.UIObject, contentWindow);

        inventoryItem.GetComponent<UIItem>().SetUIItemVisuals(item);

        inventoryUIContents.Add(inventoryItem.GetComponent<UIItem>());
    }
    //Called when an item is added to the player's inventory
    public void PickedUpItem(Item item)
    {
        for (int i = 0; i < inventoryUIContents.Count; i++)
        {
            if (inventoryUIContents[i].GetComponent<UIItem>().itemData == item)
                inventoryUIContents[i].GetComponent<UIItem>().SetUIItemVisuals(item);

        }
    }
    //Called when an item is removed from the player's inventory
    public void DroppedItem(Item item)
    {
        foreach (UIItem uiItem in inventoryUIContents)
        {
            if (uiItem.itemData == item)
            {
                Destroy(uiItem.gameObject);
                inventoryUIContents.Remove(uiItem);
                break;
            }
        }
    }
    //Called when an item is equipped in the player's inventory
    public void EquippedItem(Item item)
    {
        foreach (UISlot slot in equipmentUIPositions)
        {
            if (item.definition.equipSlot == slot.slotType)
            {
                foreach (UIItem uiItem in inventoryUIContents)
                {
                    if (uiItem.itemData == item)
                    {
                        uiItem.transform.SetParent(slot.transform);
                        uiItem.GetComponent<RectTransform>().localPosition = Vector3.zero;
                        uiItem.GetComponent<RectTransform>().sizeDelta = new Vector2(slot.GetComponent<RectTransform>().rect.width, slot.GetComponent<RectTransform>().rect.height);
                        uiItem.GetComponent<UIItem>().SetUIEquipmentVisuals(item);
                    }
                }
            }
        }
    }
    //Called when an item is unequipped in the player's inventory
    public void UnequippedItem(Item item)
    {
        foreach (UISlot slot in equipmentUIPositions)
        {
            if (slot.slotType == item.definition.equipSlot)
            {
                slot.GetComponentInChildren<UIItem>().SetUIItemVisuals(item);
                slot.GetComponentInChildren<UIItem>().transform.SetParent(contentWindow);
            }
        }
    }
}
