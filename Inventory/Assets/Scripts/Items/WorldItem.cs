using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class WorldItem : MonoBehaviour
{
    public Item itemData;

    float spawnTime = 0;

    [SerializeField] float waitToPickUpTime = 1f;

    public Rigidbody rb;
    public float throwForce = 5f;
    [SerializeField] Collider pickupTrigger;

    private void Awake()
    {
        spawnTime = Time.realtimeSinceStartup;

        //Instantiate(item.definition.worldVisuals, transform); //Spawn visuals prefab of item as child

        if (rb == null) //If no rigidbody was plugged in
            rb = GetComponent<Rigidbody>(); //Get it's own rigidbody

        pickupTrigger.enabled = false;
    }

    private void Start()
    {
        if(itemData != null)
        {
            Instantiate(itemData.definition.worldVisuals, this.transform);
        }
    }

    private void Update()
    {
        if (!pickupTrigger.enabled && Time.realtimeSinceStartup - spawnTime >= waitToPickUpTime)
            pickupTrigger.enabled = true;

        transform.Rotate(new Vector3(0, 1, 0), 45 * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Time.realtimeSinceStartup - spawnTime < waitToPickUpTime) //If the time its been spawned is shorter than the time you have to wait to pick it back up
            return; //Do nothing (Can't pick up yet)

        Inventory inventory = other.GetComponent<Inventory>(); //Get the inventory component of whatever touched the item
        if (inventory == null) //If no inventory is found
            return; //Do nothing (Can't store it anywhere)

        inventory.AddToInventory(itemData, itemData.currentStackSize); //Add the item data to the inventory

        Destroy(gameObject); //Destroy the world object
    }

}
