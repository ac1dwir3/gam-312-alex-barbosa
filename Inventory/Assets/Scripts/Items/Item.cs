using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item
{
    public int currentStackSize;
    public ItemDefinition definition;
    public UIItem uiItem;
}
