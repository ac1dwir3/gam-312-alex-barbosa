using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Ranged Weapon", menuName = "Items/Weapon/Create Ranged Weapon")]
public class RangedWeapon : Weapon
{
    public float reloadSpeed;

    private void Awake()
    {
        weaponType = WeaponType.Ranged;
    }
}
