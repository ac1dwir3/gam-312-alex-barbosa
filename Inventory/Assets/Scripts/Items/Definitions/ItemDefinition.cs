using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Items/Create Generic Item")]
public class ItemDefinition : ScriptableObject
{
    public new string name;
    public int maxStackSize;
    public float weight;

    public GameObject worldObject;
    public GameObject worldVisuals;

    public GameObject UIObject;
    public Sprite UIIcon;

    public Inventory.Slot.SlotType equipSlot = Inventory.Slot.SlotType.None;
}
