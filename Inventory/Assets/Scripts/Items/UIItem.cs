using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIItem : MonoBehaviour
{
    public Item itemData = null;

    [SerializeField] Image icon;
    [SerializeField] TextMeshProUGUI itemNameText;
    [SerializeField] TextMeshProUGUI itemStackText;
    [SerializeField] TMP_Dropdown usageDropdown;

    public void SetUIEquipmentVisuals(Item item)
    {
        itemData = item;
        itemData.uiItem = this;

        icon.sprite = item.definition.UIIcon;
        itemNameText.text = item.definition.name;
        itemStackText.text = "";
    }
    public void SetUIItemVisuals(Item item)
    {
        itemData = item;
        itemData.uiItem = this;

        icon.sprite = item.definition.UIIcon;
        itemNameText.text = item.definition.name;
        itemStackText.text = item.currentStackSize.ToString();
    }

    public void HandleUsageDropdown()
    {
        switch (usageDropdown.value)
        {
            case 0: //Drop
                GameManager.instance.inventoryUI.playerInventory.DropItem(itemData);
                break;
            case 1: //Equip
                GameManager.instance.inventoryUI.playerInventory.EquipItem(itemData);
                break;
            case 2: //Unequip
                GameManager.instance.inventoryUI.playerInventory.UnequipItem(itemData);
                break;
        }
    }
}
