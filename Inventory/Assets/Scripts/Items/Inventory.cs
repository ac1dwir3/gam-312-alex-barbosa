using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using static Inventory;

public class Inventory : MonoBehaviour
{
    // Items In The Inventory
    public List<Item> items = new List<Item>();

    [System.Serializable]
    public class Slot
    {
        public Slot(SlotType type) { filter = type; }

        public enum SlotType
        {
            None = 0, //0000
            Head = 1, //0001
            Hands = 2, //0010
            Torso = 3, //0011
            Legs = 4, //0100
            Feet = 5, //0101
            MainHand = 6, //0110
            OffHand = 7, //0111
        }
        public SlotType filter;
        public Item equippedItem = null;
    }
    public List<Slot> equippedItems = new List<Slot>();

    #region Observer Pattern / Delegates

    public delegate void dItemPickup(Item item);
    public dItemPickup itemPickupCallback;
    public dItemPickup newItemPickupCallback;
    public delegate void dItemDrop(Item item);
    public dItemDrop itemDropCallback;
    public delegate void dItemEquip(Item item);
    public dItemEquip itemEquipCallback;
    public delegate void dItemUnequip(Item item);
    public dItemUnequip itemUnequipCallback;

    #endregion

    private void OnEnable()
    {
        GameManager.instance.inputManager.addItemPressed += AddToInventory;
    }
    private void OnDisable()
    {
        GameManager.instance.inputManager.addItemPressed -= AddToInventory;
    }

    private void Awake()
    {
        #region Slot Creation
        Slot headSlot = new Slot(Slot.SlotType.Head);
        equippedItems.Add(headSlot);

        Slot handsSlot = new Slot(Slot.SlotType.Hands);
        equippedItems.Add(handsSlot);

        Slot torsoSlot = new Slot(Slot.SlotType.Torso);
        equippedItems.Add(torsoSlot);

        Slot legsSlot = new Slot(Slot.SlotType.Legs);
        equippedItems.Add(legsSlot);

        Slot feetSlot = new Slot(Slot.SlotType.Feet);
        equippedItems.Add(feetSlot);

        Slot mainHandSlot = new Slot(Slot.SlotType.MainHand);
        equippedItems.Add(mainHandSlot);

        Slot offHandSlot = new Slot(Slot.SlotType.OffHand);
        equippedItems.Add(offHandSlot);
        #endregion
    }

    public void AddToInventory(Item item)
    {
        AddToInventory(item.definition, item.currentStackSize);
    }
    public void AddToInventory(Item item, int count)
    {
        AddToInventory(item.definition, count);
    }
    public void AddToInventory(ItemDefinition item, int count)
    {
        if (count == 0) //If adding 0 items...
            return; //Just stop

        int remaining = count;
        foreach (Item _item in items) //Loop through whole inventory
        {
            if (_item.definition == item) //If there is already a stack of this item in the inventory
            {
                int numToAdd = Mathf.Min(_item.definition.maxStackSize - _item.currentStackSize, remaining); //If remaining is larger than what this stack can hold, only add what's nessecary
                if (numToAdd > 0)
                {
                    _item.currentStackSize += numToAdd; //Fills the existing stack
                    itemPickupCallback(_item);
                    remaining -= numToAdd; //Recalculates how much of this item stack is left to add to inventory
                }
            }

            if (remaining == 0) //If there is no more items to add
                break; //Stop looking for more items in the inventory to stack with
        }

        // Making new stacks of item
        while (remaining > 0) //if there is still more to add to the inventory
        {
            int currentStack = Mathf.Min(remaining, item.maxStackSize); //If remaining is larger than maxStackSize, only add maxStackSize to inventory

            Item newStackOfItem = new Item(); //Create new stack
            newStackOfItem.definition = item;
            newStackOfItem.currentStackSize = currentStack;
            items.Add(newStackOfItem); //Adds new stack to inventory

            remaining -= currentStack; //Recalculates how much of this item stack is left to add to inventory

            newItemPickupCallback(newStackOfItem);
        }
    }

    public void DropItem(Item item)
    {
        int indexOfItem = -1;
        Item itemToDrop = null;

        for (int i = 0; i < items.Count; i++) //Loops through entire inventory
        {
            if (items[i] == item) //If the item you want to drop is found in the inventory
            {
                indexOfItem = i;
                itemToDrop = items[i];
                break;
            }
        }

        if (indexOfItem > -1 && itemToDrop != null) //If the item was actually found in the inventory
        {
            foreach (Slot slot in equippedItems)
            {
                if (slot.equippedItem != null && slot.equippedItem.definition != null)
                {
                    if (slot.equippedItem == itemToDrop)
                    {
                        UnequipItem(slot.equippedItem);
                    }
                }
            }

            GameObject worldItemObject = Instantiate(itemToDrop.definition.worldObject, new Vector3(transform.position.x, transform.position.y + .5f), Quaternion.identity); //Instantiates world item of the dropped item;
            worldItemObject.GetComponent<WorldItem>().itemData = itemToDrop; //Assigns item's data to the world item
            worldItemObject.GetComponent<WorldItem>().rb.AddForce(new Vector3(0, 1, 1) * worldItemObject.GetComponent<WorldItem>().throwForce, ForceMode.Impulse); //Throw the item forward

            items.RemoveAt(indexOfItem); //Remove the item from the inventory
            Debug.Log("Dropped " + item.definition.name);
            itemDropCallback(itemToDrop);
        }
        else //If the item was not found
            Debug.LogWarning("No such item exists in the inventory.");
    }

    public void EquipItem(Item item)
    {
        bool hasItem = false;

        foreach (Item _item in items) //Checks inventory to make sure you have the item you are trying to equip
        {
            if (_item.definition == item.definition) //If you have the item in your inventory
            {
                hasItem = true;
                break;
            }
        }

        if (!hasItem) //If the item is not even in your inventory...
        {
            Debug.LogError(item.definition.name + " is not in your inventory... You cannot equip it.");
            return; //Just stop
        }

        bool equipped = false;

        if (item.definition.equipSlot == Slot.SlotType.None) //If the item is not even equippable or not set to a slot...
        {
            Debug.LogError("Item: " + item.definition.name + " is not an equippable item or hasn't been set to a valid equipment slot!");
            return; //Just stop
        }

        foreach (Slot slot in equippedItems) //Loop through all the equipment slots
        {
            if (slot.filter == item.definition.equipSlot) //If the item can be placed in an existing slot
            {
                if (slot.equippedItem != null)
                {
                    if (slot.equippedItem.definition != null) //If there is already an item equipped to that slot
                    {
                        Debug.LogWarning("Equipment Slot: " + slot.filter + " had " + slot.equippedItem.definition.name + " equipped. Now it has " + item.definition.name + " equipped.");
                        UnequipItem(slot.equippedItem); //Remove equipped item
                    }
                }


                slot.equippedItem = item; //Equip new item
                Debug.Log("Equipped " + slot.equippedItem.definition.name);
                equipped = true;
                itemEquipCallback(item);
                break;
            }
        }

        if (!equipped)
            Debug.LogWarning("Failed to equip: " + item.definition.name + "!");
    }
    public void UnequipItem(Item item)
    {
        bool itemEquipped = false;
        foreach (Slot slot in equippedItems) //Loop through all equipped items
        {
            if (slot.equippedItem != null && slot.equippedItem.definition != null)
            {
                if (slot.equippedItem == item) //If the item is currently equipped
                {
                    itemEquipped = true;
                    Debug.Log("Unequipped " + slot.equippedItem.definition.name);
                    slot.equippedItem = null; //Unequips item
                    itemUnequipCallback(item);
                    return;
                }
            }
        }

        if (!itemEquipped)
            Debug.LogWarning(item.definition.name + " isn't even equipped!");
    }
}
