using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputManager : MonoBehaviour
{
    #region Variables
    PlayerController controller;

    #region Input Variables
    [HideInInspector] public Vector2 movementInput = Vector2.zero;
    Vector3 movementDirection;
    Vector3 slopeMovementDirection;

    [HideInInspector] public bool jumped = false;
    bool sprinting = false;
    bool crouching = false;
    #endregion

    WallJump wallJump;

    #endregion
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<PlayerController>();
        wallJump = GetComponent<WallJump>();
    }

    // Update is called once per frame
    void Update()
    {
        //Flips player based on movement direction
        if (movementInput.x > 0)
            transform.localScale = Vector3.one; //Faces right
        if (movementInput.x < 0)
            transform.localScale = new Vector3(-1, 1, 1); //Faces left

        if (wallJump.wallJumpCounter <= 0 && controller.slideTimeCounter <= 0) //Ignores input while wall jumping and sliding
        {
            //Calculates movement direction
            movementDirection = transform.right * movementInput.x;
            Debug.DrawRay(transform.position, movementDirection, Color.red); //Visualizes movement direction
            //Adjusts movement direction when on a slope to be parallel to the slope
            slopeMovementDirection = Vector3.ProjectOnPlane(movementDirection, controller.slopeHit.normal);
            Debug.DrawRay(transform.position, slopeMovementDirection, Color.blue); //Visualizes sloped movement direction


            if (crouching) //if crouch button is being pressed
            {
                if(!controller.isCrouching) //If player not already crouching
                    controller.Crouch();
            }
            else //If crouch button not being pressed
                controller.Stand();

            if (sprinting) //If sprint button is being pressed
                controller.isSprinting = true;
            else //If sprint button is not being pressed
                controller.isSprinting = false;

        }
        else
        {
            wallJump.wallJumpCounter -= Time.deltaTime; //Counts down wall jump timer until we can take input again
            controller.slideTimeCounter -= Time.deltaTime; //Counts down slide timer until we can take input again
        }
    }

    private void FixedUpdate()
    {
        if (jumped) //If jump button is being pressed pressed
            controller.Jump();

        if (wallJump.wallJumpCounter <= 0 && controller.slideTimeCounter <= 0) //Ignores input while wall jumping and sliding
        {
            if (movementDirection.magnitude >= 0.1f) //If movement input threshold past
            {
                if (!controller.OnSlope()) //If on a flat surface (not sloped)
                {
                    //Allows for variable movement speed
                    if(movementDirection.magnitude >= 1)
                        controller.Move(movementDirection.normalized);
                    else
                        controller.Move(movementDirection);
                }
                else //If on a sloped surface
                {
                    //Allows for variable movement speed
                    if (movementDirection.magnitude >= 1)
                        controller.Move(slopeMovementDirection.normalized);
                    else
                        controller.Move(slopeMovementDirection);
                }
            }
        }
    }

    #region Input Reader Methods
    public void OnMove(InputAction.CallbackContext context)
    {
        movementInput = context.ReadValue<Vector2>();
    }
    public void OnCrouch(InputAction.CallbackContext context)
    {
        crouching = context.action.triggered;
    }
    public void OnJump(InputAction.CallbackContext context)
    {
        jumped = context.action.triggered;
    }
    public void OnSprint(InputAction.CallbackContext context)
    {
        sprinting = context.action.triggered;
    }
    #endregion
}
