using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.SceneManagement; (not needed yet)

public class RespawnPlayer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        collision.gameObject.transform.position = Vector3.zero; //Resets player back to start
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); //Restarts level (not needed yet)
    }
}
