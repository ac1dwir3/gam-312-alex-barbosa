using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public enum GameState
    {
        Gameplay = 0, //0000
        InInventory = 1, //0001
        InSkills = 2, //0010
        Paused = 3, //0011
    }
    public GameState gameState = GameState.Gameplay;

    public InventoryUI inventoryUI;
    public SkillsUI skillsUI;

    public Canvas playerUICanvas;

    public int skillPoints = 0;
    public float skillPointGeneratorTime = 3;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        skillPointGeneratorTime -= Time.deltaTime; //Counts down
        if (skillPointGeneratorTime <= 0)
        {
            skillPoints++; //Adds skill points
            skillPointGeneratorTime = 3; //Resets timer
        }
    }
}
