using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(StatsSystem))]
public class PlayerController : MonoBehaviour
{
    #region Variables
    [SerializeField] PlayerInputManager input;
    public Rigidbody rb;
    public StatsSystem myStats;
    public Animator animator;

    #region Movement Variables
    [Header("Movement")]
    [HideInInspector] public RaycastHit slopeHit;
    [HideInInspector] public float movementMultiplier = 10;
    [HideInInspector] public bool isSprinting = false;

    [Header("Crouching")]
    [SerializeField] Transform headCheck;
    [SerializeField] float headCheckDistance = 1f;
    [HideInInspector] public bool isCrouching = false;
    bool canStand = true;

    [Header("Jumping ")]
    [SerializeField] float airDrag = 2;
    [SerializeField] [Range(0.01f, 1)] float airMovementMultiplier = 0.3f;
    #endregion

    #region Gravity/Falling Variables
    [Header("Grounding/Falling")]
    [SerializeField] Transform groundCheck;
    [SerializeField] float groundDistance = 0.1f;
    public LayerMask groundLayer = default;
    [HideInInspector] public bool isGrounded;

    [Header("Wall Grabbing")]
    public Transform wallGrabPoint;
    public GameObject wallSlideParticles;
    [HideInInspector] public bool canWallGrab;
    [HideInInspector] public bool isWallGrabbing;
    float storedGravity;
    #endregion

    #region Visual Variables
    [Header("Colliders")]
    public GameObject standingCollider;
    public GameObject crouchCollider;
    #endregion

    [HideInInspector] public WallJump wallJump;
    [HideInInspector] public Slide slide;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        if (input == null)
            input = GetComponent<PlayerInputManager>();
        if (myStats == null)
            myStats = GetComponent<StatsSystem>();

        if (wallJump == null)
            wallJump = GetComponent<WallJump>();
        if (slide == null)
            slide = GetComponent<Slide>();

        if (rb == null)
            rb = GetComponent<Rigidbody>();

        rb.freezeRotation = true;
        myStats.ResetAll();

        storedGravity = myStats.GetCurrentValue(StatsSystem.Stat.StatType.GravityMultiplier);
        myStats.AddStatChangedCallback(UpdateGravity);
    }

    // Update is called once per frame
    void Update()
    {
        ControlDrag();

        isWallGrabbing = false;
        if (canWallGrab && !isGrounded) //If there is a wall in front of player and player is not on the ground
        {
            if ((input.movementInput.x > 0 && transform.localScale.x == 1) || (input.movementInput.x < 0 && transform.localScale.x == -1)) //If facing the wall and moving towards it
                isWallGrabbing = true;
        }

        #region Animation Variable Drivers
        animator.SetFloat("VelocityX", Mathf.Abs(rb.velocity.normalized.x));
        animator.SetFloat("VelocityY", rb.velocity.normalized.y);
        animator.SetBool("isGrounded", isGrounded);
        animator.SetBool("Crouching", isCrouching);
        animator.SetBool("Sprinting", isSprinting);
        animator.SetBool("WallGrabbing", isWallGrabbing);
        #endregion
    }

    private void FixedUpdate()
    {
        animator.SetBool("Jumped", false);
        isGrounded = Physics.CheckSphere(groundCheck.position, -groundDistance, groundLayer);
        if (!isGrounded && rb.useGravity)
        {
            rb.AddForce(Physics.gravity * myStats.GetCurrentValue(StatsSystem.Stat.StatType.GravityMultiplier), ForceMode.Acceleration);
            Debug.DrawRay(transform.position, rb.velocity.normalized, Color.green);
        }

        if (isWallGrabbing)
        {
            rb.useGravity = false; //Deactivates Gravity
            myStats.stats[StatsSystem.Stat.StatType.GravityMultiplier].currentValue = 0; //Deactivates Artificial Gravity

            rb.AddForce(Vector3.down * myStats.GetCurrentValue(StatsSystem.Stat.StatType.WallSlideForce), ForceMode.Force); //Slide player down the wall
        }
        else
        {
            rb.useGravity = true; //Activates Gravity
            myStats.stats[StatsSystem.Stat.StatType.GravityMultiplier].currentValue = storedGravity; //Activates Artificial Gravity
        }

        canWallGrab = Physics.CheckSphere(wallGrabPoint.position, 0.3f, groundLayer) && !isCrouching; //Can't grab the wall if the player is not facing the wall or if they're crouching

        if (isWallGrabbing) //If grabbing onto a wall
            wallSlideParticles.SetActive(true); //Activates particles when sliding down a wall
        else
            wallSlideParticles.SetActive(false); //Deactivates particles

        canStand = !Physics.CheckCapsule(headCheck.position, new Vector3(headCheck.position.x, headCheck.position.y + headCheckDistance, headCheck.position.z), .45f, groundLayer);
    }

    #region Methods
    /// <summary>
    /// Changes rigidbody's drag based on whether the player is in the air or on the ground
    /// </summary>
    void ControlDrag()
    {
        if (isGrounded) //If on the ground
            rb.drag = myStats.GetCurrentValue(StatsSystem.Stat.StatType.GroundDrag); //Apply ground drag
        else //If not on the ground (in the air)
            rb.drag = airDrag; //Apply air drag
    }
    /// <summary>
    /// Stores gravity value
    /// </summary>
    /// <param name="stat">Gravity stat</param>
    public void UpdateGravity(StatsSystem.Stat stat)
    {
        if (stat.type == StatsSystem.Stat.StatType.GravityMultiplier)
        {
            storedGravity = myStats.GetCurrentValue(StatsSystem.Stat.StatType.GravityMultiplier);
        }
    }
    /// <summary>
    /// Detects checks the angle of the surface the player is standing on
    /// </summary>
    /// <returns>Whether or not the player is standing on a sloped surface</returns>
    public bool OnSlope()
    {
        if (Physics.Raycast(groundCheck.position, Vector3.down, out slopeHit, groundDistance, groundLayer)) //If there is a surface below the player
        {
            if (slopeHit.normal != Vector3.up) //If the surface is not perfectly flat
                return true; //Player is standing on a slope
            else //If the surface is perfectly flat
                return false; //Player is standing on a flat surface (not on a slope)
        }

        return false; //If there is no surface below the player then the player is not on a slope
    }
    public void Move(Vector3 direction)
    {
        if (myStats.GetCurrentValue(StatsSystem.Stat.StatType.CarryWeight) >= myStats.GetMaxValue(StatsSystem.Stat.StatType.CarryWeight)) //If over-encumbered
            movementMultiplier = 5; //movementSpeed is halved
        else
            movementMultiplier = 10;

        if (isGrounded && !isSprinting && !isCrouching) //If grounded, NOT sprinting, and NOT crouching
            rb.AddForce(direction * (myStats.GetCurrentValue(StatsSystem.Stat.StatType.MovementSpeed) * movementMultiplier), ForceMode.Acceleration); //Move with normal speed
        else if (isGrounded && isSprinting && !isCrouching) //If grounded, sprinting, but not crouching
            rb.AddForce(direction * ((myStats.GetCurrentValue(StatsSystem.Stat.StatType.MovementSpeed) * movementMultiplier) * myStats.GetCurrentValue(StatsSystem.Stat.StatType.SprintSpeedMultiplier)), ForceMode.Acceleration); //Move with sprint speed modifier
        else if (isGrounded && isCrouching) //If grounded and crouching (regardless if sprinting)
            rb.AddForce(direction * ((myStats.GetCurrentValue(StatsSystem.Stat.StatType.MovementSpeed) * movementMultiplier) * myStats.GetCurrentValue(StatsSystem.Stat.StatType.CrouchSpeedMultiplier)), ForceMode.Acceleration); //Move with crouch speed modifier
        else if (!isGrounded) //If in the air
            rb.AddForce(direction * ((myStats.GetCurrentValue(StatsSystem.Stat.StatType.MovementSpeed) * movementMultiplier) * airMovementMultiplier), ForceMode.Acceleration); //Move with air speed ("resistance") modifier
    }
    public void Jump()
    {
        if (isGrounded) //No jumping while in mid-air
        {
            rb.velocity = new Vector3(rb.velocity.x, 0, 0);
            animator.SetBool("Jumped", true);

            if (!isCrouching) //If not crouching
            {
                rb.AddForce(transform.up * myStats.GetCurrentValue(StatsSystem.Stat.StatType.JumpForce), ForceMode.Impulse); //Jump normally
            }
            else //If crouching
                rb.AddForce(transform.up * (myStats.GetCurrentValue(StatsSystem.Stat.StatType.JumpForce) * myStats.GetCurrentValue(StatsSystem.Stat.StatType.CrouchJumpMultiplier)), ForceMode.Impulse); //Crouch hop
        }
    }
    public void Crouch()
    {
        isCrouching = true;

        //Change colliders
        standingCollider.SetActive(false);
        crouchCollider.SetActive(true);
    }
    public void Stand()
    {
        if (canStand) //If player has enough room above them to stand
        {
            isCrouching = false;

            //Change colliders
            crouchCollider.SetActive(false);
            standingCollider.SetActive(true);
        }
    }
    #endregion
}
