using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slide : MonoBehaviour
{
    #region Variables
    [SerializeField] PlayerController controller;

    [Tooltip("Velocity in the x direction must be greater than this threshold to slide (account for Movement Speed)")] public float slideSpeedMin = 12;
    [SerializeField] [Tooltip("How long (in seconds) you ignore player input for while sliding")] float slideTime = 0.5f;
    [HideInInspector] public float slideTimeCounter;
    [HideInInspector] public bool isSliding = false;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (controller.isGrounded && Mathf.Abs(controller.rb.velocity.x) > slideSpeedMin && controller.isCrouching && !isSliding) //If grounded and moving fast enough
        {
            DoSlide();
        }
        if (slideTimeCounter <= 0)
            isSliding = false;

        controller.animator.SetBool("Sliding", isSliding);

    }

    public void DoSlide()
    {
        isSliding = true;
        slideTimeCounter = slideTime; //Starts countdown that ignores player input

        controller.rb.velocity = new Vector3(controller.rb.velocity.normalized.x * slideSpeedMin, controller.rb.velocity.y);

        Vector3 slideDirection = controller.rb.velocity.normalized; //Records current movement direction for slide direction
        slideDirection.y = 0; //Don't slide upward

        controller.rb.AddForce(slideDirection * controller.myStats.GetCurrentValue(StatsSystem.Stat.StatType.SlideForce) * 2, ForceMode.Impulse); //Apply Slide/Dash/Dive Force
    }
}
