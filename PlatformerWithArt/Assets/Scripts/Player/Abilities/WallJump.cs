using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallJump : MonoBehaviour
{
    #region Variables
    [SerializeField] PlayerController controller;
    [SerializeField] PlayerInputManager input;

    [Header("Wall Jumping")]
    [SerializeField] [Min(10f)] Vector2 wallJumpForce;
    [Space]
    [SerializeField] [Range(0.05f, 0.3f)] [Tooltip("How long (in seconds) player input is ignored for after a wall jump")] float wallJumpTime = 0.1f;
    [HideInInspector] public float wallJumpCounter;
    bool wallJumpPreped = false;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        if (controller == null)
            controller = GetComponent<PlayerController>();
        if (input == null)
            input = GetComponent<PlayerInputManager>();

        controller.myStats.AddStatChangedCallback(UpdateWallJumpForce);

        wallJumpForce = new Vector2(controller.myStats.GetCurrentValue(StatsSystem.Stat.StatType.JumpForce), controller.myStats.GetCurrentValue(StatsSystem.Stat.StatType.JumpForce));
    }

    // Update is called once per frame
    void Update()
    {
        if (input.jumped && wallJumpPreped) //If the player can walljump and they pressed the jump button
        {
            controller.animator.SetBool("Jumped", true);
        }
    }

    private void FixedUpdate()
    {
        if (controller.isWallGrabbing) //If player is grabbing a wall
        {
            if (!input.jumped) //If the player let go of the jump button (prevents player from holding jump to walljump)
                wallJumpPreped = true;

            if (input.jumped && wallJumpPreped) //If the player can walljump and they pressed the jump button
            {
                wallJumpCounter = wallJumpTime; //Starts countdown that ignores player input
                controller.rb.velocity = Vector3.zero;
                controller.rb.AddForce(new Vector2(-input.movementInput.x * wallJumpForce.x, wallJumpForce.y), ForceMode.Impulse); //Applys walljump force
            }
        }
        else //If the player is not grabbing a wall
        {
            wallJumpPreped = false; //Unpreps the next walljump
        }
    }

    void UpdateWallJumpForce(StatsSystem.Stat stat)
    {
        if(stat.type == StatsSystem.Stat.StatType.JumpForce)
        {
            wallJumpForce = new Vector2(controller.myStats.GetCurrentValue(StatsSystem.Stat.StatType.JumpForce), controller.myStats.GetCurrentValue(StatsSystem.Stat.StatType.JumpForce));
        }
    }
}
