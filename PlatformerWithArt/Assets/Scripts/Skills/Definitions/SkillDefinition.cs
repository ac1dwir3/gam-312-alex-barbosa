using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillDefinition : ScriptableObject
{
    public new string name;

    public enum SkillType
    {
        StatModifier = 0, //0000
        Ability = 1, //0001
    }
    [HideInInspector] public SkillType skillType { get; protected set; }

    public GameObject UIObject;

    [System.Serializable]
    public class StatToModify
    {
        public StatsSystem.Stat.StatType statToModify;
        public float modifyValue = 0;
    }
    public List<StatToModify> statsToModify = new List<StatToModify>();

    [System.Serializable]
    public class AbilityToUnlock
    {
        public enum Abilities
        {
            WallJump = 0, //0000
            Slide = 1, //0001
        }
        public Abilities ability;
    }
    public List<AbilityToUnlock> abilitiesToUnlock = new List<AbilityToUnlock>();
}
