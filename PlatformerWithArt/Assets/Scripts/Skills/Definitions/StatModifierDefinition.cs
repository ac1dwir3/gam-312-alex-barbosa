using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Skill", menuName = "Skills/Create New Stat Modifier")]
public class StatModifierDefinition : SkillDefinition
{
    private void Awake()
    {
        skillType = SkillType.StatModifier;
    }
}
