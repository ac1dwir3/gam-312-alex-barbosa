using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Ability", menuName = "Skills/Create New Ability")]
public class AbilityDefinition : SkillDefinition
{
    private void Awake()
    {
        skillType = SkillType.Ability;
    }
}
