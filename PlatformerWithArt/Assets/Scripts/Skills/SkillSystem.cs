using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillSystem : MonoBehaviour
{
    [System.Serializable]
    public class Skill
    {
        public SkillDefinition definition;
        public int unlockCost;
    }
    public List<Skill> allSkills = new List<Skill>();
    [ReadOnly] public List<SkillDefinition> unlockedSkills = new List<SkillDefinition>();

    #region Observer Pattern / Delegates
    public delegate void dStatModifierUnlocked(StatsSystem.Stat.StatType statToModify, float modifyValue);
    dStatModifierUnlocked statModifierUnlockedCallback;
    public delegate void dAbilityUnlocked();
    dAbilityUnlocked abilityUnlockedCallback;

    public void AddStatModifierUnlockedCallback(dStatModifierUnlocked someFunc)
    {
        statModifierUnlockedCallback += someFunc;
    }
    public void AddAbilityUnlockedCallback(dAbilityUnlocked someFunc)
    {
        abilityUnlockedCallback += someFunc;
    }
    #endregion

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UnlockStatModifier(SkillDefinition statModifierToUnlock)
    {
        unlockedSkills.Add(statModifierToUnlock); //Adds skill to the unlocked skills list

        for (int i = 0; i < statModifierToUnlock.statsToModify.Count; i++) //Loop through all the stats this skill modifies
            statModifierUnlockedCallback(statModifierToUnlock.statsToModify[i].statToModify, statModifierToUnlock.statsToModify[i].modifyValue); //Notifies StatsSystem that stats should be updated
    }
    public void UnlockAbility(SkillDefinition abilityToUnlock)
    {
        unlockedSkills.Add(abilityToUnlock); //Adds skill to the unlocked skills list

        for (int i = 0; i < abilityToUnlock.abilitiesToUnlock.Count; i++) //Loops through all the abilities this skill adds
        {
            string abilityScriptName = abilityToUnlock.abilitiesToUnlock[i].ability.ToString(); //Stores ability's name as a string (Must match name of the script the ability correspponds to)
            gameObject.AddComponent(System.Type.GetType(abilityScriptName)); //Adds the ability's corresponding script to the object

            abilityUnlockedCallback(); //Notifies PlayerInputController script to add functionality based on new abilities
        }
    }
}
