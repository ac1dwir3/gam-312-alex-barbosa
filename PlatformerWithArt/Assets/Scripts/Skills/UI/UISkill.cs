using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UISkill : MonoBehaviour
{
    [Header("Skill Data")]
    /*[ReadOnly]*/
    public SkillsUI ui;
    /*[ReadOnly]*/
    public SkillSystem.Skill skillData = null;
    /*[ReadOnly]*/
    public bool unlocked = false;

    [Header("UI Elements")]
    [SerializeField] Image background;
    [SerializeField] TextMeshProUGUI skillNameText;
    [SerializeField] TextMeshProUGUI costText;
    [SerializeField] GameObject unlockedIndicator;
    [SerializeField] TextMeshProUGUI statModificationValueText;

    // Start is called before the first frame update
    void Start()
    {
        if (ui == null)
            ui = GetComponentInParent<SkillsUI>();

        if (skillData.definition.skillType == SkillDefinition.SkillType.StatModifier) //If this skill is a stat modifier
        {
            for (int i = 0; i < transform.childCount; i++) //Loop through all the children of this object
            {
                if (transform.GetChild(i).name == "StatModificationValueText") //Find the child with this name
                {
                    statModificationValueText = transform.GetChild(i).GetComponent<TextMeshProUGUI>();
                    break;
                }
            }
        }

        SetUISkillVisuals(skillData);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Unlock()
    {
        if (GameManager.instance.skillPoints >= skillData.unlockCost && !unlocked) //If you have enough skill points to unlock this skill and it isn't already unlocked
        {
            GetComponent<Button>().interactable = false; //Deactivate this skill so you can't unlock it multiple times
            unlocked = true;
            GameManager.instance.skillPoints -= skillData.unlockCost; //Spend skill points

            if (skillData.definition.skillType == SkillDefinition.SkillType.StatModifier) //If this skill is a stat modifier
                ui.skills.UnlockStatModifier(skillData.definition);
            else if (skillData.definition.skillType == SkillDefinition.SkillType.Ability) //If this skill is an ability
                ui.skills.UnlockAbility(skillData.definition);

            unlockedIndicator.SetActive(true);
        }
    }

    public void SetUISkillVisuals(SkillSystem.Skill data)
    {
        if (data.definition.skillType == SkillDefinition.SkillType.StatModifier)
            background.color = Color.red;
        else if (data.definition.skillType == SkillDefinition.SkillType.Ability)
            background.color = Color.blue;

        skillNameText.text = data.definition.name;
        costText.text = data.unlockCost.ToString();

        if (statModificationValueText != null) //If this skill is a stat modifier this will be true
        {
            for (int i = 0; i < data.definition.statsToModify.Count; i++) //Loop through all the stats this skill modifies
            {
                statModificationValueText.text += data.definition.statsToModify[i].statToModify.ToString() + ": " + data.definition.statsToModify[i].modifyValue; //List all stats this skill modifies and by how much
                if (i != data.definition.statsToModify.Count - 1) //If there are still more stats to list
                    statModificationValueText.text += ",\n"; //Add a comma and enter a new line
            }
        }
    }
}
