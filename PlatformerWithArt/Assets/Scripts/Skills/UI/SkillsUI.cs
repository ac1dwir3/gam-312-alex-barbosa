using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SkillsUI : MonoBehaviour
{
    public SkillSystem skills;

    [SerializeField] RectTransform contentWindow;
    [SerializeField] TextMeshProUGUI skillPointsText;

    // Start is called before the first frame update
    void Start()
    {
        if (skills == null)
            skills = GetComponentInParent<SkillSystem>();

        for (int i = 0; i < skills.allSkills.Count; i++)
        {
            GameObject uiSkill = Instantiate(skills.allSkills[i].definition.UIObject, contentWindow);
            uiSkill.GetComponent<UISkill>().ui = this;
            uiSkill.GetComponent<UISkill>().skillData = skills.allSkills[i];
        }
    }

    // Update is called once per frame
    void Update()
    {
        skillPointsText.text = "Skill Points: " + GameManager.instance.skillPoints.ToString();
    }


}
