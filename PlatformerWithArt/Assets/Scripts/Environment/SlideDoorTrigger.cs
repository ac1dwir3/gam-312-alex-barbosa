using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideDoorTrigger : MonoBehaviour
{
    OpenSlideDoor parent;

    // Start is called before the first frame update
    void Start()
    {
        parent = GetComponentInParent<OpenSlideDoor>();
    }

    private void OnTriggerEnter(Collider other)
    {
        parent.OnTriggerEnter(other);
    }

    private void OnTriggerStay(Collider other)
    {
        parent.OnTriggerStay(other);
    }

    private void OnTriggerExit(Collider other)
    {
        parent.OnTriggerExit(other);
    }
}
