using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    [SerializeField] BoxCollider door;

    private void OnTriggerStay(Collider other)
    {
        door.enabled = false;
    }

    private void OnTriggerExit(Collider other)
    {
        door.enabled = true;
    }
}
