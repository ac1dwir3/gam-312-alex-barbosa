using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(StatsSystem))]
public class Inventory : MonoBehaviour
{
    // Items In The Inventory
    public List<Item> items = new List<Item>();
    public StatsSystem myStats;

    [System.Serializable]
    public class Slot
    {
        public Slot(SlotType type) { filter = type; }

        public enum SlotType
        {
            None = 0, //0000
            Head = 1, //0001
            Hands = 2, //0010
            Torso = 3, //0011
            Legs = 4, //0100
            Feet = 5, //0101
            MainHand = 6, //0110
            OffHand = 7, //0111
        }
        public SlotType filter;
        public Item equippedItem = null;
    }
    public List<Slot> equippedItems = new List<Slot>();

    #region Observer Pattern / Delegates

    public delegate void dItemPickup(Item item);
    dItemPickup itemPickupCallback;
    public delegate void dItemDrop(Item item);
    dItemDrop itemDropCallback;
    public delegate void dItemEquip(Item item);
    dItemEquip itemEquipCallback;
    public delegate void dItemUnequip(Item item);
    dItemUnequip itemUnequipCallback;

    public void AddItemPickupCallback(dItemPickup someFunc)
    {
        itemPickupCallback += someFunc;
    }
    public void AddItemDropCallback(dItemDrop someFunc)
    {
        itemDropCallback += someFunc;
    }
    public void AddItemEquipCallback(dItemEquip someFunc)
    {
        itemEquipCallback += someFunc;
    }
    public void AddItemUnequipCallback(dItemUnequip someFunc)
    {
        itemUnequipCallback += someFunc;
    }

    #endregion

    private void Awake()
    {
        #region Slot Creation
        Slot headSlot = new Slot(Slot.SlotType.Head);
        equippedItems.Add(headSlot);

        Slot handsSlot = new Slot(Slot.SlotType.Hands);
        equippedItems.Add(handsSlot);

        Slot torsoSlot = new Slot(Slot.SlotType.Torso);
        equippedItems.Add(torsoSlot);

        Slot legsSlot = new Slot(Slot.SlotType.Legs);
        equippedItems.Add(legsSlot);

        Slot feetSlot = new Slot(Slot.SlotType.Feet);
        equippedItems.Add(feetSlot);

        Slot mainHandSlot = new Slot(Slot.SlotType.MainHand);
        equippedItems.Add(mainHandSlot);

        Slot offHandSlot = new Slot(Slot.SlotType.OffHand);
        equippedItems.Add(offHandSlot);
        #endregion

        if (myStats == null)
            GetComponent<StatsSystem>();

    }
    private void Start()
    {
        
    }

    public void AddToInventory(Item item)
    {
        AddToInventory(item.definition, item.currentStackSize);
    }
    public void AddToInventory(Item item, int count)
    {
        AddToInventory(item.definition, count);
    }
    public void AddToInventory(ItemDefinition item, int count)
    {
        if (count == 0) //If adding 0 items...
            return; //Just stop

        int remaining = count;
        foreach (Item _item in items) //Loop through whole inventory
        {
            if (_item.definition == item) //If there is already a stack of this item in the inventory
            {
                int numToAdd = Mathf.Min(_item.definition.maxStackSize - _item.currentStackSize, remaining); //If remaining is larger than what this stack can hold, only add what's nessecary
                _item.currentStackSize += numToAdd; //Fills the existing stack
                remaining -= numToAdd; //Recalculates how much of this item stack is left to add to inventory
            }

            if (remaining == 0) //If there is no more items to add
                break; //Stop looking for more items in the inventory to stack with
        }

        // Making new stacks of item
        while (remaining > 0) //if there is still more to add to the inventory
        {
            int currentStack = Mathf.Min(remaining, item.maxStackSize); //If remaining is larger than maxStackSize, only add maxStackSize to inventory

            Item newStackOfItem = new Item(); //Create new stack
            newStackOfItem.definition = item;
            newStackOfItem.currentStackSize = currentStack;
            items.Add(newStackOfItem); //Adds new stack to inventory

            remaining -= currentStack; //Recalculates how much of this item stack is left to add to inventory

            itemPickupCallback(newStackOfItem);
        }

        UpdateStats();

    }

    public void DropItem(Item item)
    {
        DropItem(item.definition);
    }
    public void DropItem(ItemDefinition item)
    {
        int indexOfItem = -1;
        Item itemToDrop = null;

        for (int i = 0; i < items.Count; i++) //Loops through entire inventory
        {
            if (items[i].definition == item) //If the item you want to drop is found in the inventory
            {
                indexOfItem = i;
                itemToDrop = items[i];
                break;
            }
        }

        if (indexOfItem > -1 && itemToDrop != null) //If the item was actually found in the inventory
        {
            foreach (Slot slot in equippedItems)
            {
                if (slot.equippedItem != null && slot.equippedItem.definition != null)
                {
                    if (slot.equippedItem.definition == itemToDrop.definition)
                    {
                        UnequipItem(slot.equippedItem);
                    }
                }
            }

            GameObject worldItemObject = Instantiate(itemToDrop.definition.worldObject, new Vector3(transform.position.x, transform.position.y + .5f), Quaternion.identity); //Instantiates world item of the dropped item;
            worldItemObject.GetComponent<WorldItem>().itemData = itemToDrop; //Assigns item's data to the world item

            items.RemoveAt(indexOfItem); //Remove the item from the inventory
            itemDropCallback(itemToDrop);
            UpdateStats();
        }
        else //If the item was not found
            Debug.LogWarning("No such item exists in the inventory.");

    }

    public void EquipItem(Item item)
    {
        bool hasItem = false;

        foreach (Item _item in items) //Checks inventory to make sure you have the item you are trying to equip
        {
            if (_item.definition == item.definition) //If you have the item in your inventory
            {
                hasItem = true;
                break;
            }
        }

        if (!hasItem) //If the item is not even in your inventory...
        {
            Debug.LogError(item.definition.name + " is not in your inventory... You cannot equip it.");
            return; //Just stop
        }

        bool equipped = false;

        if (item.definition.usablity != ItemDefinition.Usability.Equippable || item.definition.equipSlot == Slot.SlotType.None) //If the item is not even equippable or not set to a slot...
        {
            Debug.LogError("Item: " + item.definition.name + " is not an equippable item or hasn't been set to a valid equipment slot!");
            return; //Just stop
        }

        foreach (Slot slot in equippedItems) //Loop through all the equipment slots
        {
            if (slot.filter == item.definition.equipSlot) //If the item can be placed in an existing slot
            {
                if (slot.equippedItem != null)
                {
                    if (slot.equippedItem.definition != null) //If there is already an item equipped to that slot
                    {
                        Debug.LogWarning("Equipment Slot: " + slot.filter + " had " + slot.equippedItem.definition.name + " equipped. Now it has " + item.definition.name + " equipped.");
                        UnequipItem(slot.equippedItem); //Remove equipped item
                    }
                }


                slot.equippedItem = item; //Equip new item
                Debug.Log("Equipped " + slot.equippedItem.definition.name);
                equipped = true;
                itemEquipCallback(item);
                UpdateStats(); //Update equipper's stats
                break;
            }
        }

        if (!equipped)
            Debug.LogWarning("Failed to equip: " + item.definition.name + "!");
    }
    public void UnequipItem(Item item)
    {
        bool itemEquipped = false;
        foreach (Slot slot in equippedItems) //Loop through all equipped items
        {
            if (slot.equippedItem != null && slot.equippedItem.definition != null)
            {
                if (slot.equippedItem.definition == item.definition) //If the item is currently equipped
                {
                    itemEquipped = true;
                    Debug.Log("Unequipped " + slot.equippedItem.definition.name);
                    slot.equippedItem = null; //Unequips item
                    itemUnequipCallback(item);
                    UpdateStats();
                    return;
                }
            }
        }

        if (!itemEquipped)
            Debug.LogWarning(item.definition.name + " isn't even equipped!");
    }

    public void UpdateStats()
    {
        myStats.ResetAll();
        foreach (Item item in items)
        {
            myStats.ChangeCurrentValue(StatsSystem.Stat.StatType.CarryWeight, item.definition.weight);
        }

        foreach (Slot slot in equippedItems)
        {
            if (slot.equippedItem != null && slot.equippedItem.definition != null)
            {
                if (slot.equippedItem.definition.IsType(ItemDefinition.ItemType.Wearable))
                {
                    for (int i = 0; i < slot.equippedItem.definition.statsToModify.Count; i++)
                    {
                        myStats.ChangeCurrentValue(slot.equippedItem.definition.statsToModify[i].stat, slot.equippedItem.definition.statsToModify[i].modifyValue);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Sorts the inventory alphabetically by the items' names
    /// </summary>
    public void SortAlphabetically()
    {
        //Preserve all the names of the items in the inventory (Uppercase/Lowercase and Punctuation)
        string[] savedNames = new string[items.Count];
        for (int i = 0; i < savedNames.Length; i++)
        {
            savedNames[i] = items[i].definition.name;
        }

        //Makes duplicate list of items with all names changed to lowercase
        List<Item> itemsLower = items;
        for (int i = 0; i < itemsLower.Count; i++)
        {
            itemsLower[i].definition.name = itemsLower[i].definition.name.ToLower();
        }

        //Sorts lowercased list alphabetically
        for (int i = 0; i < itemsLower.Count - 1; i++)
        {
            for (int j = 0; j < itemsLower.Count - 1; j++)
            {
                if (itemsLower[j].definition.name.ElementAt(0) > itemsLower[j + 1].definition.name.ElementAt(0))
                {
                    Item temp = itemsLower[j];
                    itemsLower[j] = itemsLower[j + 1];
                    itemsLower[j + 1] = temp;
                }
            }
        }

        //Assigns items to this now-sorted lowercased list
        items = itemsLower;

        //Restores the items names to their original form (Uppercases the letters that were originally uppercased)
        for (int i = 0; i < items.Count; i++)
        {
            items[i].definition.name = savedNames[i];
        }
    }
}
