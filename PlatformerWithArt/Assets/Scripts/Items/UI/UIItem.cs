using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIItem : MonoBehaviour
{
    public Item itemData = null;

    [SerializeField] Image icon;
    [SerializeField] TextMeshProUGUI itemNameText;
    [SerializeField] TextMeshProUGUI itemStackText;
    [SerializeField] Button dropButton;
    [SerializeField] Button UnequipButton;

    public void SetUIEquipmentVisuals(Item item)
    {
        itemData = item;

        icon.sprite = item.definition.UIIcon;
        itemNameText.text = item.definition.name;
        itemStackText.text = "";
        dropButton.gameObject.SetActive(false);
        UnequipButton.gameObject.SetActive(true);
    }
    public void SetUIItemVisuals(Item item)
    {
        itemData = item;

        icon.sprite = item.definition.UIIcon;
        itemNameText.text = item.definition.name;
        itemStackText.text = item.currentStackSize.ToString();
        dropButton.gameObject.SetActive(true);
        UnequipButton.gameObject.SetActive(false);
    }

    public void Drop()
    {
        GameManager.instance.inventoryUI.playerInventory.DropItem(itemData);
    }
    public void Unequip()
    {
        GameManager.instance.inventoryUI.playerInventory.UnequipItem(itemData);
    }
}
