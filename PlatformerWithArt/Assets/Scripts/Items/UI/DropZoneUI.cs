using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DropZoneUI : MonoBehaviour, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        DraggableUI dragged = eventData.pointerDrag.GetComponent<DraggableUI>();

        if (dragged.GetComponent<UIItem>().itemData.definition.equipSlot != Inventory.Slot.SlotType.None)
        {
            GameManager.instance.inventoryUI.playerInventory.EquipItem(dragged.GetComponent<UIItem>().itemData);
            dragged.parentToReturnTo = this.transform;
            Destroy(dragged.placeHolder);

            dragged.GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
    }
}
