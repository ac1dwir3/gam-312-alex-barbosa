using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class WorldItem : MonoBehaviour
{
    public Item itemData;

    [SerializeField] ItemDefinition defaultDefinition;

    float spawnTime = 0;

    [SerializeField] float waitToPickUpTime = 1f;

    [SerializeField] Rigidbody rb;
    [SerializeField] float throwForce = 5f;
    [SerializeField] new SphereCollider collider;

    PlayerController player;

    private void Awake()
    {
        spawnTime = Time.realtimeSinceStartup;

        //Instantiate(item.definition.worldVisuals, transform); //Spawn visuals prefab of item as child

        if (rb == null) //If no rigidbody was plugged in
            rb = GetComponent<Rigidbody>(); //Get it's own rigidbody

        collider = GetComponent<SphereCollider>(); //Get it's own collider
        collider.enabled = false;

        if (itemData.definition == null)
            itemData.definition = defaultDefinition;
    }

    private void Start()
    {
        player = FindObjectOfType<PlayerController>();

        Vector3 throwDirection = player.transform.localScale;
        throwDirection.z = 0;

        rb.AddForce(throwDirection * throwForce, ForceMode.Impulse); //Throw the item in the direction the dropper is facing

        if(itemData == null)
        {
            itemData = new Item();
            itemData.definition = defaultDefinition;
            itemData.currentStackSize = defaultDefinition.maxStackSize;
        }
    }

    private void Update()
    {
        if (Time.realtimeSinceStartup - spawnTime >= waitToPickUpTime)
            collider.enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Time.realtimeSinceStartup - spawnTime < waitToPickUpTime) //If the time its been spawned is shorter than the time you have to wait to pick it back up
            return; //Do nothing (Can't pick up yet)

        if (other.attachedRigidbody == null) //If the collision object does NOT have a rigidbody
            return; //Do Nothing (Won't have ability to pick up item)

        Inventory inventory = other.attachedRigidbody.GetComponent<Inventory>(); //Get the inventory component of whatever touched the item
        if (inventory == null) //If no inventory is found
            return; //Do nothing (Can't store it anywhere)

        inventory.AddToInventory(itemData, itemData.currentStackSize); //Add the item data to the inventory

        Destroy(gameObject); //Destroy the world object
    }

}
