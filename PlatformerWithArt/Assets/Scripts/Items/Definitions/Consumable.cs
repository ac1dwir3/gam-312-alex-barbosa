using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Healing Consumable", menuName = "Items/Consumables/Create Healing Consumable")]
public class Consumable : ItemDefinition
{
    private void Awake()
    {
        type = ItemType.Consumable;
        usablity = Usability.Consumable;

    }
}
