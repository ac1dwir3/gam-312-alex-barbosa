using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsSystem : MonoBehaviour
{
    [System.Serializable]
    public class Stat
    {
        public enum StatType
        {
            JumpForce = 1,
            MovementSpeed = 2,
            CarryWeight = 3,
            SlideForce = 4,
            SprintSpeedMultiplier = 5,
            CrouchSpeedMultiplier = 6,
            CrouchJumpMultiplier = 7,
            GroundDrag = 8, //Can affect slipperiness of stat bearer
            GravityMultiplier = 9, //Can make stat bearer fall faster/slower
            WallSlideForce = 10, //Can make stat bearer slide down walls faster, climb up walls, or stick in place
        }
        public StatType type;
        public float baseValue;
        [ReadOnly] public float currentValue;
        public float minValue;
        public float maxValue;
        public bool uncappedMax;
        public bool uncappedMin;
    }
    public Dictionary<Stat.StatType, Stat> stats = new Dictionary<Stat.StatType, Stat>();

    [SerializeField] List<Stat> initialStatValues = new List<Stat>();

    SkillSystem skills;

    #region Observer Pattern / Delegates
    public delegate void dStatChanged(Stat stat);
    dStatChanged statChangedCallback;

    public void AddStatChangedCallback(dStatChanged someFunc)
    {
        statChangedCallback += someFunc;
    }
    #endregion

    private void Start()
    {
        if (skills == null)
            skills = GetComponent<SkillSystem>();

        if (skills)
        {
            skills.AddStatModifierUnlockedCallback(ChangeBaseValue);
        }

        for (int i = 0; i < initialStatValues.Count; i++)
        {
            initialStatValues[i].currentValue = initialStatValues[i].baseValue;
            stats.Add(initialStatValues[i].type, initialStatValues[i]);
        }
    }

    /// <param name="statToGet">The desired stat</param>
    /// <returns>The current value of the stat</returns>
    public float GetCurrentValue(Stat.StatType statToGet)
    {
        if (stats.ContainsKey(statToGet))
            return stats[statToGet].currentValue;
        else
            Debug.LogWarning(this.name + " does not have a " + statToGet.ToString() + " stat!");

        return 0;
    }
    /// <param name="statToGet">The desired stat</param>
    /// <returns>The maximum value the stat can be</returns>
    public float GetMaxValue(Stat.StatType statToGet)
    {
        if (stats.ContainsKey(statToGet))
            return stats[statToGet].maxValue;
        else
            Debug.LogWarning(this.name + " does not have that stat!");

        return 0;
    }
    /// <param name="statToGet">The desired stat</param>
    /// <returns>The minimum value the stat can be</returns>
    public float GetMinValue(Stat.StatType statToGet)
    {
        if (stats.ContainsKey(statToGet))
            return stats[statToGet].minValue;
        else
            Debug.LogWarning(this.name + " does not have that stat!");

        return 0;
    }

    /// <summary>
    /// Changes the base value of a stat
    /// </summary>
    /// <param name="statToChange">The desired stat you want to change</param>
    /// <param name="valueOfChange">The amount of change you want to make to the stat's base value (negative numbers for subtraction/positive numbers for addition)</param>
    void ChangeBaseValue(Stat.StatType statToChange, float valueOfChange)
    {
        bool statExists = false;
        for (int i = 0; i < initialStatValues.Count; i++)
        {
            if (initialStatValues[i].type == statToChange)
            {
                statExists = true;
                break;
            }
        }

        if (statExists)
        {
            for (int i = 0; i < initialStatValues.Count; i++)
            {
                if (initialStatValues[i].type == statToChange)
                {
                    if (!initialStatValues[i].uncappedMax && !initialStatValues[i].uncappedMin)
                    {
                        if ((initialStatValues[i].baseValue + valueOfChange) >= initialStatValues[i].maxValue) //If the stat change is greater than the stat's max value
                            initialStatValues[i].baseValue = initialStatValues[i].maxValue; //Set the stat's base value to it's max value
                        else if ((initialStatValues[i].baseValue + valueOfChange) <= initialStatValues[i].minValue) //If the stat change is less than the stat's min value
                            initialStatValues[i].baseValue = initialStatValues[i].minValue; //Set the stat's base value to it's min value
                        else //If the stat change is NOT greater than it's max value AND is NOT less than it's min value
                            initialStatValues[i].baseValue += valueOfChange; //Set base value of the stat to the new value
                    }
                    else if (!initialStatValues[i].uncappedMax) //If the stat is NOT allowed to surpass it's max and min values
                    {
                        if ((initialStatValues[i].baseValue + valueOfChange) >= initialStatValues[i].maxValue) //If the stat change is greater than the stat's max value
                            initialStatValues[i].baseValue = initialStatValues[i].maxValue; //Set the stat's base value to it's max value
                        else //If the stat change is NOT greater than it's max value
                            initialStatValues[i].baseValue += valueOfChange; //Set base value of the stat to the new value
                    }
                    else if (!initialStatValues[i].uncappedMin)
                    {
                        if ((initialStatValues[i].baseValue + valueOfChange) <= initialStatValues[i].minValue) //If the stat change is less than the stat's min value
                            initialStatValues[i].baseValue = initialStatValues[i].minValue; //Set the stat's base value to it's min value
                        else //If the stat change is NOT less than it's min value
                            initialStatValues[i].baseValue += valueOfChange; //Set currebasent value of the stat to the new value
                    }
                    else //If the stat IS allowed to surpass it's max AND min values
                    {
                        initialStatValues[i].baseValue += valueOfChange; //Set base value of the stat to the new value
                    }
                }
            }

            for (int i = 0; i < initialStatValues.Count; i++)
            {
                if (initialStatValues[i].type == statToChange)
                {
                    statChangedCallback(initialStatValues[i]);
                    ResetAll();
                    break;
                }
            }
        }
        else
            Debug.LogWarning(this.name + " does not have that stat!");
    }

    /// <summary>
    /// Changes the current value of a stat
    /// </summary>
    /// <param name="statToChange">The desired stat you want to change</param>
    /// <param name="valueOfChange">The amount of change you want to make to the stat's current value (negative numbers for subtraction/positive numbers for addition)</param>
    public void ChangeCurrentValue(Stat.StatType statToChange, float valueOfChange)
    {
        bool statExists = false;
        if (stats.ContainsKey(statToChange))
            statExists = true;

        if (statExists)
        {
            if (!stats[statToChange].uncappedMax && !stats[statToChange].uncappedMin)
            {
                if ((stats[statToChange].currentValue + valueOfChange) >= stats[statToChange].maxValue) //If the stat change is greater than the stat's max value
                    stats[statToChange].currentValue = stats[statToChange].maxValue; //Set the stat's current value to it's max value
                else if ((stats[statToChange].currentValue + valueOfChange) <= stats[statToChange].minValue) //If the stat change is less than the stat's min value
                    stats[statToChange].currentValue = stats[statToChange].minValue; //Set the stat's current value to it's min value
                else //If the stat change is NOT greater than it's max value AND is NOT less than it's min value
                    stats[statToChange].currentValue += valueOfChange; //Set current value of the stat to the new value
            }
            else if (!stats[statToChange].uncappedMax) //If the stat is NOT allowed to surpass it's max and min values
            {
                if ((stats[statToChange].currentValue + valueOfChange) >= stats[statToChange].maxValue) //If the stat change is greater than the stat's max value
                    stats[statToChange].currentValue = stats[statToChange].maxValue; //Set the stat's current value to it's max value
                else //If the stat change is NOT greater than it's max value
                    stats[statToChange].currentValue += valueOfChange; //Set current value of the stat to the new value
            }
            else if (!stats[statToChange].uncappedMin)
            {
                if ((stats[statToChange].currentValue + valueOfChange) <= stats[statToChange].minValue) //If the stat change is less than the stat's min value
                    stats[statToChange].currentValue = stats[statToChange].minValue; //Set the stat's current value to it's min value
                else //If the stat change is NOT less than it's min value
                    stats[statToChange].currentValue += valueOfChange; //Set current value of the stat to the new value
            }
            else //If the stat IS allowed to surpass it's max AND min values
            {
                stats[statToChange].currentValue += valueOfChange; //Set current value of the stat to the new value
            }

            statChangedCallback(stats[statToChange]);
        }
        else
            Debug.LogWarning(this.name + " does not have that stat!");
    }
    /// <summary>
    /// Resets a stat's current value to it's initial value
    /// </summary>
    /// <param name="statToReset">The desired stat you want reset</param>
    public void ResetStat(Stat.StatType statToReset)
    {
        bool statExists = false;
        if (stats.ContainsKey(statToReset))
            statExists = true;

        if (statExists)
        {
            for (int i = 0; i < initialStatValues.Count; i++)
            {
                if (initialStatValues[i].type == statToReset)
                {
                    stats[statToReset].currentValue = initialStatValues[i].baseValue;
                    statChangedCallback(stats[statToReset]);
                    break;
                }
            }
        }
        else
            Debug.LogWarning(this.name + " does not have that stat!");
    }
    /// <summary>
    /// Resets all stats to their initial values
    /// </summary>
    public void ResetAll()
    {
        for (int i = 0; i < stats.Count; i++)
        {
            stats[initialStatValues[i].type].currentValue = initialStatValues[i].baseValue;
            statChangedCallback(stats[initialStatValues[i].type]);
        }
    }
}
